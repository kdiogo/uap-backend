# Standard Library Imports
import os
import re
from setuptools import setup, PEP420PackageFinder

# External Library Imports

# Module Imports

NAME = 'uap-backend'
DESCRIPTION = "Universal Audio Platform Backend Source Code"
REQUIRES_PYTHON = ">=3.6"


REQUIRED = [
    "invoke==1.4.1",
    "fastapi[all]==0.63.0",
    "httpx==0.16.1",
    "SQLAlchemy==1.3.20",
    "bcrypt==3.2.0",
    "spotipy",
    "python-dotenv",
    "pyjwt",
    "python-multipart"
]

TEST_REQUIRED = [
    "pylint",
    "pytest"
]

# Pull version from __version__.py file
VERSION_REGEX = re.compile(r"__version__= '(\d+.\d+.\d+)'")
VERSION_REGEX = re.compile(VERSION_REGEX)

HERE = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(HERE, 'app', '__version__.py')) as f:
    VERSION = re.search(VERSION_REGEX, f.read()).group(1)

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author="Kenny Diogo & Nick Petho",
    author_email="kenneth.diogo@gmail.com",
    url="https://bitbucket.org/kdiogo/uap-backend",
    python_requires=REQUIRED,
    packages=PEP420PackageFinder.find(include=['app', 'app.*']),
    install_requires=REQUIRED,
    extra_require={
        'test': TEST_REQUIRED
    },
    include_package_data=True
)
