# Standard Library Imports
import typing

# External Library Imports
import bcrypt
from sqlalchemy import (
    Column, Integer, String, ForeignKey
)
from sqlalchemy.orm.exc import DetachedInstanceError
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.declarative import declarative_base

# Module Imports


class Base(object):
    def __repr__(self) -> str:
        return self._repr(id=self.id)

    def _repr(self, **fields: typing.Dict[str, typing.Any]) -> str:
        """
        Helper for __repr__
        """
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f'{key}={field!r}')
            except DetachedInstanceError:
                field_strings.append(f'{key}=DetachedInstanceError')
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"


Base = declarative_base(cls=Base)


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)

    username = Column(String(16), nullable=False, unique=True)
    _password = Column(String(128))

    firstname = Column(String(32))
    lastname = Column(String(32))
    blends = relationship("Blend", back_populates="user")

    spotify_refresh_token = Column(String, nullable=True)

    @hybrid_property
    def password(self):
        return self._password

    @password.setter
    def password(self, password: str):
        self._password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())

    def verify_password(self, password: str):
        return bcrypt.checkpw(password.encode('utf-8'), self.password)

    def __repr__(self):
        return self._repr(
            id=self.id,
            username=self.username,
            firstname=self.firstname,
            lastname=self.lastname
        )


class Blend(Base):
    __tablename__ = 'blend'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship("User", back_populates="blends")
    tracks = relationship("BlendTrack", back_populates="blend")

    def __repr__(self):
        return self._repr(
            id=self.id,
            name=self.name,
            user_id=self.user_id,
            user=self.user,
            tracks=self.tracks
        )


class BlendTrack(Base):
    __tablename__ = 'blend_track'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    artist = Column(String, nullable=False)
    album = Column(String, nullable=False)
    rank = Column(Integer, nullable=False)
    service = Column(String, nullable=False)
    service_track_id = Column(String, nullable=False)

    blend_id = Column(Integer, ForeignKey('blend.id'), nullable=False)
    blend = relationship("Blend", back_populates="tracks")

    def __repr__(self):
        return self._repr(
            id=self.id,
            name=self.name,
            artist=self.artist,
            album=self.album,
            rank=self.rank,
            service=self.service,
            service_track_id=self.service_track_id
        )

