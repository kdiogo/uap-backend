# Standard Library Imports

# External Library Imports
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, sessionmaker

# Module Imports
from app.config import settings
from app.db import models

ENGINE: Engine = create_engine(settings.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True)
session_factory = sessionmaker(autocommit=False, autoflush=False, bind=ENGINE)


def init_db(db: Session) -> None:
    models.Base.metadata.create_all(bind=ENGINE)
