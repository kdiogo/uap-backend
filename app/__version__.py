"""
Defines version number for the project. Used in setup.py
"""
__version__ = "0.0.1"
