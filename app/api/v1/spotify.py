# Standard Library Imports
import logging
from datetime import datetime, timedelta

# External Library Imports
import httpx
from sqlalchemy.orm import Session
from fastapi import APIRouter, HTTPException, status, Depends

# Module Imports
from app.config import settings
from app.api import deps
from app import crud
from app.schemas import (
    User,
    ConnectSpotifyParams, RefreshSpotifyTokenResponse, GetSpotifyTokenResponse,
)

router = APIRouter()

LOGGER = logging.getLogger(__name__)
SPOTIFY_TOKEN_ENDPOINT = "https://accounts.spotify.com/api/token"


@router.post("/connect", response_model=RefreshSpotifyTokenResponse)
async def connect_to_spotify(
        *,
        db: Session = Depends(deps.get_db),
        current_user: User = Depends(deps.current_user),
        params: ConnectSpotifyParams
):
    """
    Gets access token and refresh tokens from Spotify and stores them for the current user.
    Returns the access token.
    """

    async with httpx.AsyncClient() as client:
        data = {
            "grant_type": "authorization_code",
            "code": params.code,
            "redirect_uri": params.redirect_uri,
            "client_id": settings.SPOTIFY_CLIENT_ID,
            "client_secret": settings.SPOTIFY_CLIENT_SECRET
        }
        r = await client.post(SPOTIFY_TOKEN_ENDPOINT, data=data)
        if r.status_code == status.HTTP_400_BAD_REQUEST:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=r.json()["error_description"])
        if r.status_code != status.HTTP_200_OK:
            LOGGER.error("Error while retrieving access token from Spotify: %s", r.text)
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Something went wrong")

    response = GetSpotifyTokenResponse.parse_obj(r.json())
    user_db = crud.user.get(db, current_user.id)
    user_db.spotify_refresh_token = response.refresh_token
    db.commit()

    return RefreshSpotifyTokenResponse(
        access_token=response.access_token,
        token_type=response.token_type,
        scope=response.scope,
        expires_in=response.expires_in
    )


@router.delete("/refresh")
def disconnect_spotify(
        db: Session = Depends(deps.get_db),
        current_user: User = Depends(deps.current_user)
):
    user_db = crud.user.get(db, current_user.id)
    user_db.spotify_refresh_token = None
    db.commit()
    return {"detail": "Successfully deleted Spotify auth data."}


@router.post("/refresh", response_model=RefreshSpotifyTokenResponse)
async def refresh_spotify_token(
        current_user: User = Depends(deps.current_user)
):
    """
    Refreshes Spotify issued access token
    """

    if not current_user.spotify_refresh_token:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="User not yet connected with Spotify.")

    async with httpx.AsyncClient() as client:
        data = {
            "grant_type": "refresh_token",
            "refresh_token": current_user.spotify_refresh_token,
            "client_id": settings.SPOTIFY_CLIENT_ID,
            "client_secret": settings.SPOTIFY_CLIENT_SECRET
        }
        r = await client.post(SPOTIFY_TOKEN_ENDPOINT, data=data)
        if r.status_code == status.HTTP_400_BAD_REQUEST:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=r.json()["error_description"])
        if r.status_code != status.HTTP_200_OK:
            LOGGER.error("Error while refreshing access token from Spotify: %s", r.text)
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Something went wrong")

    return r.json()


