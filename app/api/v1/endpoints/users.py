# Standard Library Imports
from typing import List

# External Library Imports
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

# Module Imports
from app.api import deps
from app import schemas
from app import crud
from app.config import settings

router = APIRouter()


@router.post("/", response_model=schemas.User)
def create_user(
        *,
        db: Session = Depends(deps.get_db),
        user_in: schemas.UserCreate
):
    if crud.user.get(db, user_in.username, column="username"):
        raise HTTPException(status.HTTP_400_BAD_REQUEST, f"Username taken.")

    return crud.user.create(db, user_in)

