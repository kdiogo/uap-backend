# Standard Library Imports
from typing import List

# External Library Imports
from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.orm import Session

# Module Imports
from app.api import deps
from app import schemas
from app import crud
from app.crud.utils.filters import FilterItem, ComparisonOperator
from app.config import settings

router = APIRouter()


@router.post("/", response_model=schemas.Blend)
def create_blend(
        *,
        db: Session = Depends(deps.get_db),
        current_user: schemas.User = Depends(deps.current_user),
        blend_in: schemas.BlendBase
):
    user_db = crud.user.get(db, current_user.username, column="username")

    blend_create = schemas.BlendCreate(
        user_id=user_db.id,
        name=blend_in.name,
        tracks=blend_in.tracks
    )
    return crud.blend.create(db, blend_create)


@router.get("/", response_model=List[schemas.Blend])
def get_user_blends(
        *,
        db: Session = Depends(deps.get_db),
        current_user: schemas.User = Depends(deps.current_user)
):
    filters = {
        "user_id": [
            FilterItem(operator=ComparisonOperator.EQUAL, operand=current_user.id)
        ]
    }
    blends = crud.blend.get_multi(db, filters=filters)
    return blends


@router.get("/{id}", response_model=schemas.Blend)
def get_user_blend(
        *,
        id: str,
        db: Session = Depends(deps.get_db),
        current_user: schemas.User = Depends(deps.current_user)
):
    blend = crud.blend.get(db, id)
    return blend
