# Standard Library Imports

# External Library Imports
from fastapi import APIRouter

# Module Imports
from app.api.v1.endpoints import users, blends

api_router = APIRouter()

api_router.include_router(users.router, prefix="/user", tags=["user"])
api_router.include_router(blends.router, prefix="/blend", tags=["blend"])
