# Standard Library Imports
from datetime import datetime, timedelta

# External Library Imports
import jwt
from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

# Module Imports
from app.schemas import User, LoginResponse
from app import crud
from app.api import deps
from app.config import settings

router = APIRouter()


def create_token(payload: dict, expires_minutes: int):
    """
    Returns an encoded JWT string.
    """

    to_encode = payload.copy()
    expire = datetime.utcnow() + timedelta(minutes=expires_minutes)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.UAP_SECRET, algorithm=settings.JWT_ALGORITHM)
    return encoded_jwt


@router.post('/token', response_model=LoginResponse)
def login(
        db: Session = Depends(deps.get_db),
        form_data: OAuth2PasswordRequestForm = Depends()
):
    # Check if user exists
    user = crud.user.get(db, form_data.username, column='username')
    if not user:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password.")

    # Verify password
    if not user.verify_password(form_data.password):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password.")

    access_token_payload = {
        "username": user.username,
        "firstname": user.firstname,
        "lastname": user.lastname,
        "token_type": "access"
    }

    refresh_token_payload = {
        "token_type": "refresh"
    }
    return LoginResponse(
        access_token=create_token(access_token_payload, 30),
        refresh_token=create_token(refresh_token_payload, 90),
        token_type="bearer"
    )


@router.post("/refresh")
def refresh_token():
    pass


@router.get("/current_user", response_model=User)
def get_current_user(
        current_user: User = Depends(deps.current_user)
):
    return current_user

