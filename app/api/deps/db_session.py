# Standard Library Imports
from typing import Generator

# External Library Imports

# Module Imports
from app.db import session_factory


def get_db() -> Generator:
    """
    Injectable dependency that provides a SQLAlchemy session.
    """

    try:
        db = session_factory()
        yield db
    finally:
        db.close()
