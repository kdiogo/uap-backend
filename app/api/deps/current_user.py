# Standard Library Imports
import logging
from base64 import b64decode

# External Library Imports
import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session

# Module Imports
from app import crud
from app.config import settings
from app.schemas import AccessTokenPayload, User
from app.api.deps.db_session import get_db

oauth_scheme = OAuth2PasswordBearer(tokenUrl='token')
LOGGER = logging.getLogger(__name__)


def current_user(db: Session = Depends(get_db), token: str = Depends(oauth_scheme)) -> User:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials.",
        headers={"WWW-Authenticate": "Bearer"}
    )

    try:
        payload = AccessTokenPayload.parse_obj(
            jwt.decode(token, settings.UAP_SECRET, algorithms=[settings.JWT_ALGORITHM])
        )
    except jwt.ExpiredSignatureError as e:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Token expired."
        ) from e
    except jwt.InvalidTokenError as e:
        LOGGER.error("Could not decode JWT: %s", str(e))
        raise credentials_exception from e

    user = crud.user.get(db, payload.username, column="username")
    if not user:
        LOGGER.warning("Username from JWT not found: %s", payload.username)
        raise credentials_exception

    return User.from_orm(user)
