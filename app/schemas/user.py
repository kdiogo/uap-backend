# Standard Library Imports
from typing import Optional

# External Library Imports
from pydantic import BaseModel

# Module Imports


class UserBase(BaseModel):
    username: str
    firstname: Optional[str]
    lastname: Optional[str]


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    spotify_refresh_token: Optional[str]

    class Config:
        orm_mode = True
