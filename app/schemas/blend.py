# Standard Library Imports
from typing import List

# External Library Imports
from pydantic import BaseModel

# Module Imports
from .uap import SupportedService


class BlendTrack(BaseModel):
    name: str
    artist: str
    album: str
    service: SupportedService
    service_track_id: str
    rank: int


class BlendTrackInDB(BlendTrack):
    class Config:
        orm_mode = True


class BlendBase(BaseModel):
    name: str
    tracks: List[BlendTrack]


class BlendCreate(BlendBase):
    user_id: int


class Blend(BlendCreate):
    id: int
    tracks: List[BlendTrackInDB]

    class Config:
        orm_mode = True
