"""
Type definitions for BeatBop authorization parameters/responses
"""

# Standard Library Imports
from typing import Optional

# External Library Imports
from pydantic import BaseModel

# Module Imports


class LoginResponse(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str


class AccessTokenPayload(BaseModel):
    username: str
    firstname: Optional[str]
    lastname: Optional[str]


class ConnectSpotifyParams(BaseModel):
    code: str
    redirect_uri: str




