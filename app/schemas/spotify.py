"""
Type definitions for Spotify API responses
"""

# Standard Library Imports

# External Library Imports
from pydantic import BaseModel

# Module Imports


class RefreshSpotifyTokenResponse(BaseModel):
    access_token: str
    token_type: str
    scope: str
    expires_in: int


class GetSpotifyTokenResponse(RefreshSpotifyTokenResponse):
    refresh_token: str


