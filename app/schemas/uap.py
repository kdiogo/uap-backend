# Standard Library Imports
from enum import Enum

# External Library Imports

# Module Imports


class SupportedService(str, Enum):
    SPOTIFY = "spotify"
