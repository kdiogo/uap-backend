# Standard Library Imports
import secrets

# External Library Imports
from pydantic import BaseSettings

# Module Imports


class Settings(BaseSettings):
    """
    Provides configurable settings for the FastAPI application.
    Each property listed here is also exposed and subject to change from env variables.
    """

    API_V1_PREFIX: str = "/api/v1"
    SERVER_URI: str = "http://localhost:8000"
    UAP_SECRET: str = None
    JWT_ALGORITHM: str = "HS256"
    ACCESS_TOKEN_EXPIRE_MINUTES = 30
    REFRESH_TOKEN_EXPIRE_MINUTES = 90

    # Database Configurations
    SQLALCHEMY_DATABASE_URI: str = "sqlite:///uap.db?check_same_thread=False"

    # Streaming Service Configurations
    SPOTIFY_CLIENT_ID: str = None
    SPOTIFY_CLIENT_SECRET: str = None

    class Config:
        case_sensitive = True
        env_file = '.env'
        env_file_encoding = 'utf-8'


settings = Settings()
