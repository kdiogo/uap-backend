# Standard Library Imports

# External Library Imports
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

# Module Imports
from app.crud.base import CRUDBase
from app.db.models import Blend, BlendTrack
from app import schemas


class CRUDBlend(CRUDBase[Blend, Blend, Blend]):
    """
    Database operations for user model
    """

    def create(self, db: Session, obj_in: schemas.BlendCreate) -> Blend:
        blend_data = jsonable_encoder(obj_in)
        blend_data["tracks"] = []
        for track in obj_in.tracks:
            track_data = jsonable_encoder(track)
            track_db = BlendTrack(**track_data)
            blend_data["tracks"].append(track_db)

        blend_db = Blend(**blend_data)
        db.add(blend_db)
        db.commit()
        db.refresh(blend_db)
        return blend_db


blend = CRUDBlend(Blend)
