# Standard Library Imports

# External Library Imports

# Module Imports
from app.crud.base import CRUDBase
from app.db.models import User


class CRUDUser(CRUDBase[User, User, User]):
    """
    Database operations for user model
    """


user = CRUDUser(User)
