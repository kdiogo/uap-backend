# Standard Library Imports
import operator
from typing import Union, Any, Dict, List
from enum import Enum

# External Library Imports
from pydantic import BaseModel
from sqlalchemy.orm.query import Query

# Module Imports


class ComparisonOperator(Enum):
    GREATER = '>'
    LESS = '<'
    GREATER_EQUAL = '>='
    LESS_EQUAL = '<='
    EQUAL = '=='
    CONTAINS = 'CONTAINS'
    IN = 'IN'


class FilterItem(BaseModel):
    operator: ComparisonOperator
    operand: Union[str, int, float, bool, list]


OperationMapping = {
    ComparisonOperator.GREATER: operator.gt,
    ComparisonOperator.LESS: operator.lt,
    ComparisonOperator.GREATER_EQUAL: operator.ge,
    ComparisonOperator.LESS_EQUAL: operator.le,
    ComparisonOperator.EQUAL: operator.eq
}


def apply_filters(
        model: Any,
        query: Query,
        filters: Dict[str, List[FilterItem]]
) -> Query:
    """
    Generates SQLAlchemy query given a valid set of filter objects
    """

    for attr, filter_items in filters.items():
        for filter_item in filter_items:
            operand1 = getattr(model, attr)
            operand2 = filter_item.operand
            if filter_item.operator is ComparisonOperator.CONTAINS:
                query = query.filter(operand1.contains(operand2))
            elif filter_item.operator is ComparisonOperator.IN:
                query = query.filter(operand1.in_(operand2))
            else:
                op_func = OperationMapping[filter_item.operator]
                query = query.filter(op_func(operand1, operand2))

    return query
