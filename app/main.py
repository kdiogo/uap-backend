# Standard Library Imports

# External Library Imports
from fastapi import FastAPI

# Module Imports
from app.api.v1 import api_router
from app.api.v1 import auth, spotify
from app.config import settings
from app.db import init_db, session_factory

app = FastAPI()

app.include_router(
    api_router,
    prefix=settings.API_V1_PREFIX
)

app.include_router(
    auth.router
)

app.include_router(
    spotify.router,
    prefix="/spotify"
)

# Initialize Database
db = session_factory()
init_db(db)


# Misc. Endpoints
@app.get("/")
def status():
    """
    Test endpoint to verify status of server
    """
    return {"status": "ok"}

