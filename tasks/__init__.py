from invoke import task


@task
def dev(context):
    context.run("uvicorn app.main:app --reload")
